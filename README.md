# Shell Command

Subprocess execution library for Kotlin.

Features:
- Shell safe quoting of commands.
- Timeouts.
- File locking to prevent concurrent execution.
- Interactive commands and streaming output to stdout.
- Output capture.

## Tutorial

```kotlin
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import org.sapota.marek.shell_command.ShellSpecialCharacter
import java.io.File

val cmd1 = ShellCommand("cat", "foo", "b a r")
// Command that can be copied and pasted into a shell.
cmd1.toString()
// "cat foo b\ a\ r"
// Execute the command.
ShellExecutor().run(cmd1)

// Special characters without quoting, redirect output to file
val cmd2 = ShellCommand("echo", "foo")
cmd2.add(ShellSpecialCharacter.REDIRECT_OUTPUT)
cmd2.add("file")
cmd2.toString()
// "echo foo > file"

// Limit to one concurrent execution
val lockFile = File("lock/file")
val executor1 = ShellExecutor(exclusiveLock = lockFile)
executor1.run(cmd1)
```
