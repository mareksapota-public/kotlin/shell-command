// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import java.lang.Process

class ShellExecutorIntegrationTest {
    lateinit var process: Process
    lateinit var processBuilder: ProcessBuilder
    lateinit var environment: MutableMap<String, String>

    companion object {
        const val TIMEOUT: Long = 5
    }

    @Test
    fun testOutputCapture() = runBlocking {
        val cmd = ShellCommand("bash", "-c", "echo hello")
        val executor = ShellExecutor(timeout = TIMEOUT, captureOutput = true)
        executor.run(cmd)
        Assertions.assertEquals("hello", executor.getStdout())
        Assertions.assertEquals("", executor.getStderr())
    }

    @Test
    fun testErrorCapture() = runBlocking {
        val cmd = ShellCommand("bash", "-c", "echo hello 1>&2")
        val executor = ShellExecutor(timeout = TIMEOUT, captureOutput = true)
        executor.run(cmd)
        Assertions.assertEquals("", executor.getStdout())
        Assertions.assertEquals("hello", executor.getStderr())
    }
}
