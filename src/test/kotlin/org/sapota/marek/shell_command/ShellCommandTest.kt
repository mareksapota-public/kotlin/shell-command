// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import com.google.gson.Gson
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellSpecialCharacter

class ShellCommandTest {
    val quotingStringTests = listOf(
        Pair(arrayOf("foo", "bar"), "foo bar"),
        Pair(arrayOf("fo o", "ba r"), "fo\\ o ba\\ r"),
        Pair(arrayOf("f;oo", "bar"), "f\\;oo bar"),
        Pair(arrayOf("f'oo", "b\"ar"), "f\\'oo b\\\"ar"),
        Pair(arrayOf("f|oo", "b|ar", "|"), "f\\|oo b\\|ar \\|"),
    )

    @TestFactory
    fun testQuoting() = quotingStringTests.map({ testCase ->
        val input = testCase.first
        var inputString = Gson().toJson(input)
        val expected = testCase.second
        DynamicTest.dynamicTest(
            "Test '$inputString' is quoted to '$expected' (varargs)",
        ) {
            val shellCommand = ShellCommand(*input)
            Assertions.assertEquals(expected, shellCommand.toString())
        }
        DynamicTest.dynamicTest(
            "Test '$inputString' is quoted to '$expected' (iterable)",
        ) {
            val shellCommand = ShellCommand(input.asIterable())
            Assertions.assertEquals(expected, shellCommand.toString())
        }
    })

    @Test
    fun testNextedQuoting() {
        val innerCommand = ShellCommand("foo", "bar")
        val outerCommand = ShellCommand("baz")
        outerCommand.add(innerCommand)
        outerCommand.add("qux")
        Assertions.assertEquals(
            "baz foo\\ bar qux",
            outerCommand.toString(),
        )
        val moreOuterCommand = ShellCommand()
        moreOuterCommand.add(outerCommand)
        moreOuterCommand.add(innerCommand)
        Assertions.assertEquals(
            "baz\\ foo\\\\\\ bar\\ qux foo\\ bar",
            moreOuterCommand.toString()
        )
    }

    @Test
    fun testPipeQuoting() {
        val command = ShellCommand("ls", "-1")
        command.add(ShellSpecialCharacter.PIPE)
        command.addAll("grep", "foo")
        val wrapCommand = ShellCommand("bash", "-c")
        wrapCommand.add(command)
        val commandArray = wrapCommand.toStringList()
        Assertions.assertEquals(3, commandArray.size)
        Assertions.assertEquals("bash", commandArray[0])
        Assertions.assertEquals("-c", commandArray[1])
        Assertions.assertEquals("ls -1 | grep foo", commandArray[2])
        Assertions.assertEquals(
            "bash -c ls\\ -1\\ \\|\\ grep\\ foo",
            wrapCommand.toString(),
        )
    }

    @TestFactory
    fun testSpecialCharacters() = enumValues<ShellSpecialCharacter>().forEach {
        val command = ShellCommand("foo")
        command.add(it)
        command.add("bar" + it.argument)
        val correct = "foo ${it.argument} bar\\${it.argument}"
        var inputString = Gson().toJson(command.toStringList())
        DynamicTest.dynamicTest(
            "Test $inputString is quoted to $correct",
        ) {
            Assertions.assertEquals(correct, command.toString())
        }
    }

    @Test
    fun testCombinedCommand() {
        var command = ShellCommand("foo", "bar")
        var otherCommand = ShellCommand("baz", "qux")
        command.addAll(otherCommand)
        Assertions.assertEquals(
            "foo bar baz qux",
            command.toString(),
        )
    }

    val quotingArrayTests = listOf(
        Pair(arrayOf("foo", "bar"), listOf("foo", "bar")),
        Pair(arrayOf("fo o", "b;ar"), listOf("fo o", "b;ar")),
    )

    @TestFactory
    fun testArrayQuoting() = quotingArrayTests.map({ testCase ->
        val input = testCase.first
        var inputString = Gson().toJson(input)
        val expected = testCase.second
        var expectedString = Gson().toJson(expected)
        DynamicTest.dynamicTest(
            "Test '$inputString' is quoted to '$expectedString'"
        ) {
            val shellCommand = ShellCommand(*input)
            val output = shellCommand.toStringList()
            Assertions.assertEquals(expected.size, output.size)
            expected.zip(output) { expectedItem, outputItem ->
                Assertions.assertEquals(expectedItem, outputItem)
            }
        }
    })

    val shellWrapTests = listOf(
        Pair(arrayOf("foo", "bar"), "bash -c set\\ -euo\\ pipefail\\ \\;\\ foo\\ bar"),
        Pair(arrayOf("f oo", "bar"), "bash -c set\\ -euo\\ pipefail\\ \\;\\ f\\\\\\ oo\\ bar"),
    )

    @TestFactory
    fun testShellWrap() = shellWrapTests.map({ testCase ->
        val input = testCase.first
        val expected = testCase.second
        DynamicTest.dynamicTest(
            "Test '$input' is auto wrapped to '$expected'",
        ) {
            var command = ShellCommand(*input)
            var result = command.shellWrap()
            Assertions.assertEquals(expected, result.toString())
        }
    })
}
