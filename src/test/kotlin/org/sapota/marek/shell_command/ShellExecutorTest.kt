// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import io.mockk.every
import io.mockk.mockkClass
import io.mockk.mockkConstructor
import io.mockk.unmockkAll
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import org.sapota.marek.shell_command.ShellSpecialCharacter
import org.sapota.marek.shell_command.SudoSettings
import org.sapota.marek.shell_command.exceptions.NoOutputException
import org.sapota.marek.shell_command.exceptions.ProcessFailureException
import org.sapota.marek.shell_command.exceptions.ProcessTimeoutException
import java.io.File
import java.lang.Process
import java.util.concurrent.TimeUnit
import kotlin.streams.asStream

class ShellExecutorTest {
    lateinit var process: Process
    lateinit var processBuilder: ProcessBuilder
    lateinit var environment: MutableMap<String, String>

    companion object {
        const val TIMEOUT: Long = 555
    }

    @BeforeEach
    fun beforeEach() {
        val processHandle = mockkClass(ProcessHandle::class)
        every { processHandle.destroy() } returns true

        process = mockkClass(Process::class)
        every { process.destroy() } returns Unit
        every { process.descendants() } returns
            sequenceOf(processHandle).asStream()
        setProcessExitCode(process, 0, false)

        environment = mutableMapOf("USER" to "testuser")
        processBuilder = mockkClass(ProcessBuilder::class)
        every { processBuilder.start() } returns process
        every { processBuilder.environment() } returns environment
        every {
            processBuilder.redirectOutput(any<ProcessBuilder.Redirect>())
        } returns processBuilder
        every {
            processBuilder.redirectError(any<ProcessBuilder.Redirect>())
        } returns processBuilder
        every {
            processBuilder.redirectInput(any<ProcessBuilder.Redirect>())
        } returns processBuilder
        every {
            processBuilder.directory(any<File>())
        } returns processBuilder

        mockkConstructor(ShellExecutor::class)
        every {
            anyConstructed<ShellExecutor>().getProcessBuilder(any())
        } returns processBuilder
    }

    fun setProcessExitCode(
        proc: Process,
        exitCode: Int,
        timeout: Boolean
    ) {
        every { proc.waitFor(any(), any()) } returns (!timeout)
        every { proc.waitFor() } returns exitCode
        every { proc.exitValue() } returns exitCode
    }

    @AfterEach
    fun afterEach() {
        unmockkAll()
    }

    @Test
    fun testSuccessExecution() = runBlocking {
        val cmd = ShellCommand("ls", "-al")
        val executor = ShellExecutor(timeout = TIMEOUT)
        executor.run(cmd)
        verify(exactly = 0) { processBuilder.directory() }
        verify(exactly = 0) { processBuilder.environment() }
        verify(exactly = 0) { process.destroy() }
        verify(exactly = 1) { process.waitFor(TIMEOUT, TimeUnit.SECONDS) }
    }

    @Test
    fun testTimeoutExecution() = runBlocking {
        setProcessExitCode(process, 1, true)
        val cmd = ShellCommand("ls", "-al")
        val executor = ShellExecutor(timeout = TIMEOUT)
        Assertions.assertThrows(ProcessTimeoutException::class.java) {
            runBlocking {
                executor.run(cmd)
            }
        }
        verify(exactly = 0) { processBuilder.directory() }
        verify(exactly = 0) { processBuilder.environment() }
        verify(exactly = 1) { process.destroy() }
        verify(exactly = 1) { process.waitFor(TIMEOUT, TimeUnit.SECONDS) }
    }

    @Test
    fun testNoTimeoutExecution() = runBlocking {
        val cmd = ShellCommand("ls", "-al")
        val executor = ShellExecutor(timeout = null)
        executor.run(cmd)
        verify(exactly = 0) { processBuilder.directory() }
        verify(exactly = 0) { processBuilder.environment() }
        verify(exactly = 0) { process.destroy() }
        verify(exactly = 1) { process.waitFor() }
    }

    @Test
    fun testErrorExecution() {
        setProcessExitCode(process, 1, false)
        val cmd = ShellCommand("ls", "-al")
        val executor = ShellExecutor(timeout = TIMEOUT)
        Assertions.assertThrows(ProcessFailureException::class.java) {
            runBlocking {
                executor.run(cmd)
            }
        }
        verify(exactly = 0) { processBuilder.directory() }
        verify(exactly = 0) { processBuilder.environment() }
        verify(exactly = 0) { process.destroy() }
        verify(exactly = 1) { process.waitFor(TIMEOUT, TimeUnit.SECONDS) }
    }

    @Test
    fun testCWDExecution() = runBlocking {
        val cmd = ShellCommand("ls", "-al")
        val cwd = mockkClass(File::class)
        val executor = ShellExecutor(timeout = TIMEOUT, cwd = cwd)
        executor.run(cmd)
        verify(exactly = 1) { processBuilder.directory(cwd) }
        verify(exactly = 0) { processBuilder.environment() }
        verify(exactly = 0) { process.destroy() }
    }

    @Test
    fun testEnvExecution() = runBlocking {
        val cmd = ShellCommand("ls", "-al")
        val env = mapOf("foo" to "bar", "baz" to "qux")
        val executor = ShellExecutor(timeout = TIMEOUT, environment = env)
        executor.run(cmd)
        verify(exactly = 0) { processBuilder.directory() }
        verify(exactly = 1) { processBuilder.environment() }
        verify(exactly = 0) { process.destroy() }
        Assertions.assertEquals(
            mapOf("foo" to "bar", "baz" to "qux", "USER" to "testuser"),
            environment
        )
    }

    @Test
    fun testDefaultSudoExecution() = runBlocking {
        val cmd = ShellCommand("ls", "-al")
        val executor = ShellExecutor(sudo = SudoSettings("testuser"))
        executor.run(cmd)
        verify(exactly = 0) { processBuilder.directory() }
        verify(exactly = 0) { processBuilder.environment() }
        verify(exactly = 0) { process.destroy() }
        verify(exactly = 1) { process.waitFor() }
        Assertions.assertEquals(
            mapOf("USER" to "testuser"),
            environment
        )
        verify(exactly = 1) {
            anyConstructed<ShellExecutor>().getProcessBuilder(
                listOf(
                    "sudo",
                    "-u",
                    "testuser",
                    "--",
                    "ls",
                    "-al",
                )
            )
        }
    }

    class SudoTestCase(
        val input: Array<String>,
        val environment: Map<String, String>?,
        val envToKeep: Set<String>,
        val expected: List<String>
    )
    val sudoTestCases = listOf(
        SudoTestCase(
            arrayOf("ls", "-al"),
            null,
            setOf(),
            listOf("sudo", "-u", "testuser", "--", "ls", "-al")
        ),
        SudoTestCase(
            arrayOf("ls", "-al"),
            null,
            setOf("FOO"),
            listOf(
                "sudo",
                "-u",
                "testuser",
                "--preserve-env=FOO",
                "--",
                "ls",
                "-al",
            )
        ),
        SudoTestCase(
            arrayOf("ls", "-al"),
            null,
            setOf("FOO", "BAR", "BAZ"),
            listOf(
                "sudo",
                "-u",
                "testuser",
                "--preserve-env=BAR,BAZ,FOO",
                "--",
                "ls",
                "-al",
            )
        ),
        SudoTestCase(
            arrayOf("ls", "-al"),
            mapOf("FOO" to "foo"),
            setOf("BAZ"),
            listOf(
                "sudo",
                "-u",
                "testuser",
                "--preserve-env=BAZ,FOO",
                "--",
                "ls",
                "-al",
            )
        ),
        SudoTestCase(
            arrayOf("ls", "-al"),
            mapOf("FOO" to "foo", "BAR" to "bar"),
            setOf("BAZ"),
            listOf(
                "sudo",
                "-u",
                "testuser",
                "--preserve-env=BAR,BAZ,FOO",
                "--",
                "ls",
                "-al",
            )
        ),
        SudoTestCase(
            arrayOf("ls", "-al"),
            mapOf("FOO" to "foo", "BAR" to "bar"),
            setOf(),
            listOf(
                "sudo",
                "-u",
                "testuser",
                "--preserve-env=BAR,FOO",
                "--",
                "ls",
                "-al",
            )
        )
    )

    @TestFactory
    fun testSudoExecution() = sudoTestCases.map({ testCase ->
        val input = testCase.input
        val environment = testCase.environment
        val envToKeep = testCase.envToKeep
        val expected = testCase.expected
        DynamicTest.dynamicTest(
            "Test '${input.toList()}' with environment '$environment', " +
                "sudo environment '$envToKeep' " +
                "produces command '$expected'",
        ) {
            runBlocking {
                beforeEach()
                val executor = ShellExecutor(
                    timeout = TIMEOUT,
                    environment = environment,
                    sudo = SudoSettings("testuser", envToKeep),
                )
                executor.run(ShellCommand(*input))

                verify(exactly = 1) {
                    anyConstructed<ShellExecutor>().getProcessBuilder(expected)
                }
                afterEach()
            }
        }
    })

    @Test
    fun testNoCapture() = runBlocking {
        val cmd = ShellCommand("ls", "-al")
        val executor = ShellExecutor()
        executor.run(cmd)
        Assertions.assertThrows(NoOutputException::class.java) {
            executor.getStdout()
        }
        Assertions.assertThrows(NoOutputException::class.java) {
            executor.getStderr()
        }
    }

    @Test
    fun testAutoWrap() = runBlocking {
        val cmd = ShellCommand("pwd")
        cmd.add(ShellSpecialCharacter.PIPE)
        cmd.add("tee")
        val executor = ShellExecutor(autoShellWrap = true)
        executor.run(cmd)

        verify(exactly = 1) {
            anyConstructed<ShellExecutor>().getProcessBuilder(
                listOf(
                    "bash",
                    "-c",
                    "set -euo pipefail ; pwd | tee",
                ),
            )
        }
    }
}
