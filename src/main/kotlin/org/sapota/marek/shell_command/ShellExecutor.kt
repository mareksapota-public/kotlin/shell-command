// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.shell_command

import org.sapota.marek.shell_command.exceptions.NoOutputException
import org.sapota.marek.shell_command.exceptions.ProcessFailureException
import org.sapota.marek.shell_command.exceptions.ProcessTimeoutException
import org.sapota.marek.shell_command.lock.withLock
import org.sapota.marek.shell_command.util.BareAsterisk
import java.io.BufferedReader
import java.io.File
import java.lang.ProcessBuilder.Redirect
import java.util.concurrent.TimeUnit

open class ShellExecutor(
    @Suppress("UNUSED_PARAMETER") vararg useKeywordArgs: BareAsterisk,
    val timeout: Long? = null,
    val environment: Map<String, String>? = null,
    val cwd: File? = null,
    val sudo: SudoSettings? = null,
    val captureOutput: Boolean = false,
    val exclusiveLock: File? = null,
    val lockWait: Boolean = false,
    val autoShellWrap: Boolean = false,
) {
    private var stdout: String? = null
    private var stderr: String? = null

    suspend fun run(command: ShellCommand) {
        if (exclusiveLock !== null) {
            exclusiveLock.withLock(lockWait) {
                runCommand(command)
            }
        } else {
            runCommand(command)
        }
    }

    private fun terminate(process: Process) {
        process.descendants().forEach({ it.destroy() })
        process.destroy()
    }

    private fun runCommand(command: ShellCommand) {
        stdout = null
        stderr = null
        var cmd = command
        if (autoShellWrap && cmd.usesSpecialCharacters()) {
            cmd = cmd.shellWrap()
        }
        if (sudo !== null) {
            val sudoCmd = ShellCommand("sudo", "-u", sudo.username)
            var envToKeep = sudo.envToKeep
            if (environment !== null) {
                envToKeep += environment.keys
            }
            if (!envToKeep.isEmpty()) {
                sudoCmd.add(
                    "--preserve-env=" +
                        envToKeep.sorted().joinToString(separator = ","),
                )
            }
            sudoCmd.add("--")
            cmd = sudoCmd.addAll(cmd)
        }
        val processBuilder = getProcessBuilder(cmd.toStringList())
        if (!captureOutput) {
            processBuilder.redirectOutput(Redirect.INHERIT)
                .redirectError(Redirect.INHERIT)
                .redirectInput(Redirect.INHERIT)
        }
        if (cwd != null) {
            processBuilder.directory(cwd)
        }
        if (environment != null) {
            val processEnvironment = processBuilder.environment()
            processEnvironment.putAll(environment)
        }
        val process = processBuilder.start()
        val shutdownTerm = Thread {
            terminate(process)
        }
        Runtime.getRuntime().addShutdownHook(shutdownTerm)
        val finished = if (timeout !== null) {
            process.waitFor(timeout, TimeUnit.SECONDS)
        } else {
            process.waitFor()
            true
        }
        Runtime.getRuntime().removeShutdownHook(shutdownTerm)
        if (!finished) {
            terminate(process)
            throw ProcessTimeoutException()
        }
        if (captureOutput) {
            setOutput(process)
        }
        if (process.exitValue() != 0) {
            throw ProcessFailureException()
        }
    }

    private fun setOutput(process: Process) {
        val outReader: BufferedReader = process.inputStream.bufferedReader()
        val outBuilder = StringBuilder()
        outReader.forEachLine { line: String -> outBuilder.append(line) }
        stdout = outBuilder.toString()
        val errReader: BufferedReader = process.errorStream.bufferedReader()
        val errBuilder = StringBuilder()
        errReader.forEachLine { line: String -> errBuilder.append(line) }
        stderr = errBuilder.toString()
    }

    private fun getOut(outString: String?): String {
        val out = outString
        if (out === null) {
            throw NoOutputException()
        } else {
            return out
        }
    }

    fun getStdout(): String {
        return getOut(stdout)
    }

    fun getStderr(): String {
        return getOut(stderr)
    }

    // This method exists and is public to allow testing and mocking.
    fun getProcessBuilder(cmd: List<String>): ProcessBuilder {
        return ProcessBuilder(cmd)
    }
}
