// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.shell_command

open class ShellCommand() {
    private var command: MutableList<ShellArgument> = mutableListOf()

    private fun initCommand(arguments: Iterable<String>) {
        command = arguments.map { ShellArgument(it) }.toMutableList()
    }

    constructor(vararg arguments: String) : this() {
        initCommand(arguments.toList())
    }

    constructor(arguments: Iterable<String>) : this() {
        initCommand(arguments)
    }

    override fun toString(): String {
        return command.map { it.quoted }
            .joinToString(" ")
    }

    fun toStringList(): List<String> {
        return command.map { it.raw }
    }

    fun shellWrap(): ShellCommand {
        val wrapCommand = ShellCommand("bash", "-c")
        val innerCommand = ShellCommand("set", "-euo", "pipefail")
        innerCommand.add(ShellSpecialCharacter.SEMICOLON)
        innerCommand.addAll(this)
        wrapCommand.add(innerCommand)
        return wrapCommand
    }

    fun usesSpecialCharacters(): Boolean {
        return this.command.fold(false) { anySpecial, command ->
            anySpecial || command.isSpecial()
        }
    }

    fun add(argument: String): ShellCommand {
        command.add(ShellArgument(argument))
        return this
    }

    fun add(special: ShellSpecialCharacter): ShellCommand {
        command.add(ShellArgument(special))
        return this
    }

    fun add(otherCommand: ShellCommand): ShellCommand {
        add(otherCommand.toString())
        return this
    }

    fun addAll(arguments: Iterable<String>): ShellCommand {
        command.addAll(arguments.map { ShellArgument(it) })
        return this
    }

    @JvmName("addAll_Array_String")
    fun addAll(arguments: Array<String>): ShellCommand {
        addAll(arguments.asIterable())
        return this
    }

    @JvmName("addAll_Iterable_ShellArgument")
    fun addAll(arguments: Iterable<ShellArgument>): ShellCommand {
        command.addAll(arguments)
        return this
    }

    @JvmName("addAll_Array_ShellArgument")
    fun addAll(arguments: Array<ShellArgument>): ShellCommand {
        addAll(arguments.asIterable())
        return this
    }

    fun addAll(vararg arguments: String): ShellCommand {
        addAll(arguments.toList())
        return this
    }

    fun addAll(otherCommand: ShellCommand): ShellCommand {
        addAll(otherCommand.command)
        return this
    }
}
