// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.shell_command.lock

import kotlinx.coroutines.delay
import org.sapota.marek.shell_command.exceptions.LockFailedException
import java.io.File
import java.io.RandomAccessFile
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.channels.OverlappingFileLockException
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

private val SLEEP_TIME: Duration =
    10.toDuration(DurationUnit.MILLISECONDS)

fun FileChannel.tryLockOrNull(): FileLock? {
    return try {
        this.tryLock()
    } catch (_: OverlappingFileLockException) {
        null
    }
}

private tailrec suspend fun lockChannelWait(channel: FileChannel): FileLock {
    val lock = channel.tryLockOrNull()
    return when (lock) {
        null -> {
            delay(SLEEP_TIME)
            lockChannelWait(channel)
        }
        else -> lock
    }
}

suspend fun File.withLock(wait: Boolean, block: suspend () -> Unit) {
    RandomAccessFile(this, "rw").getChannel().let { channel ->
        val lock = if (wait) {
            lockChannelWait(channel)
        } else {
            channel.tryLockOrNull()
        }
        if (lock != null) {
            try {
                block()
            } finally {
                lock.release()
            }
        } else {
            throw LockFailedException()
        }
    }
}

suspend fun File.withLock(block: suspend () -> Unit) =
    this.withLock(true, block)
